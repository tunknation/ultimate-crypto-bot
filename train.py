import envs
import os
import argparse
import yaml

from brain import SACDAgent
from utils import make_env


def run(args):
    with open(os.path.join('config', 'sacd.yaml')) as f:
        agent_conf = yaml.load(f, Loader=yaml.SafeLoader)

    with open(os.path.join('config', 'market-trading.yaml')) as f:
        env_conf = yaml.load(f, Loader=yaml.SafeLoader)

    agent = SACDAgent(
        env_fn=lambda: make_env('MarketTrading-v0', clip_rewards=False, env_args=env_conf),
        name=args.name,
        epochs=args.epochs,
        workers=args.workers,
        seed=args.seed,
        render=args.render,
        **agent_conf)
    agent.run()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('name', nargs='?', default=None)
    parser.add_argument('-e', '--epochs', type=int, required=True)
    parser.add_argument('-w', '--workers', type=int, default=1)
    parser.add_argument('-s', '--seed', type=int, default=0)
    parser.add_argument('-r', '--render', action='store_true')
    args = parser.parse_args()
    run(args)
