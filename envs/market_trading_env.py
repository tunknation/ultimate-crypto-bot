import numpy as np
from gym.spaces import Discrete, Box

from envs.base_env import BaseEnvironment
from broker.order import MarketOrder


class MarketTradingEnv(BaseEnvironment):
    id = 'MarketTrading-v0'
    description = "Environment where agent can select market orders only"
    continuous = False

    def __init__(self, **kwargs):
        """
        Environment designed to trade price jumps using market orders.

        :param kwargs: refer to BaseEnvironment.py
        """
        super().__init__(**kwargs)

        # Environment attributes to override in sub-class
        self.actions = np.eye(3, dtype=np.float32)

        if self.continuous:
            self.action_space = Box(low=-1.,
                                    high=1.,
                                    shape=(1,),
                                    dtype=np.float32)
        else:
            self.action_space = Discrete(len(self.actions))

        # Reset to load observation.shape
        self.reset()

        self.observation_space = Box(low=-10.,
                                     high=10.,
                                     shape=self.observation.shape,
                                     dtype=np.float32)

        # Add the remaining labels for the observation space
        self.viz.observation_labels += [f'Action #{a}' for a in range(len(self.actions))]
        self.viz.observation_labels += ['Reward']

        print('{} {} #{} instantiated\nobservation_space: {}'.format(
            MarketTradingEnv.id, self.symbol, self._seed, self.observation_space.shape), 'max_steps = {}'.format(
            self.max_steps))

    def __str__(self):
        return '{} | {}-{}'.format(MarketTradingEnv.id, self.symbol, self._seed)

    def map_action_to_broker(self, action: int) -> (float, float):
        """
        Create or adjust orders per a specified action and adjust for penalties.

        :param action: (int) current step's action
        :return: (float) reward
        """
        action_penalty_reward = pnl = 0.0

        if action == 0:  # do nothing
            action_penalty_reward += self.encouragement

        elif action == 1:  # buy
            # Deduct transaction costs
            if self.transaction_fee:
                pnl -= self.market_order_fee

            if self.broker.short_inventory_count > 0:
                # Net out existing position
                order = MarketOrder(ccy=self.symbol, side='short', price=self.best_ask,
                                    step=self.local_step_number)
                pnl += self.broker.remove(order=order)
            elif self.broker.long_inventory_count >= 0:
                order = MarketOrder(ccy=self.symbol, side='long', price=self.best_ask,
                                    step=self.local_step_number)
                if self.broker.add(order=order) is False:
                    action_penalty_reward -= self.encouragement
            else:
                raise ValueError(('gym_crypto_trading.get_reward() Error for action #{} - '
                                  'unable to place an order with broker').format(action))

        elif action == 2:  # sell
            # Deduct transaction costs
            if self.transaction_fee:
                pnl -= self.market_order_fee

            if self.broker.long_inventory_count > 0:
                # Net out existing position
                order = MarketOrder(ccy=self.symbol, side='long', price=self.best_bid,
                                    step=self.local_step_number)
                pnl += self.broker.remove(order=order)
            elif self.broker.short_inventory_count >= 0:
                order = MarketOrder(ccy=self.symbol, side='short', price=self.best_bid,
                                    step=self.local_step_number)
                if self.broker.add(order=order) is False:
                    action_penalty_reward -= self.encouragement
            else:
                raise ValueError(('gym_crypto_trading.get_reward() Error for action #{} - '
                                  'unable to place an order with broker').format(action))

        else:
            raise ValueError(('Unknown action to take in get_reward(): '
                              'action={} | midpoint={}').format(action, self.midpoint))

        return action_penalty_reward, pnl

    def _map_continuous_action_to_discrete(self, action: float) -> int:
        """
        Convert continuous action to discrete equivalent.

        :param action: (float) action
        :return: (int) discrete action number
        """
        self._check_action_validity(action)

        if (self.continuous and -0.33 <= action <= 0.33) or (not self.continuous and action == 0):
            return 0

        if (self.continuous and -1.0 <= action < -0.33) or (not self.continuous and action == 1):
            return 1

        if (self.continuous and 0.33 < action <= 1) or (not self.continuous and action == 2):
            return 2

    def _create_position_features(self) -> np.ndarray:
        """
        Create an array with features related to the agent's inventory.

        :return: (np.array) normalized position features
        """
        return np.array((self.broker.net_inventory_count / self.max_position,
                         self.broker.realized_pnl * self.broker.pct_scale,
                         self.broker.get_unrealized_pnl(self.best_bid, self.best_ask)
                         * self.broker.pct_scale),
                        dtype=np.float32)

    @staticmethod
    def get_action_meanings():
        return {0: 'NOOP', 1: 'BUY', 2: 'SELL'}


class MarketTradingContinuousEnv(MarketTradingEnv):
    id = 'MarketTradingContinuous-v0'
    description = "Environment where agent can select market orders only"
    continuous = True

    def __init__(self, **kwargs):
        super(MarketTradingContinuousEnv, self).__init__(**kwargs)
