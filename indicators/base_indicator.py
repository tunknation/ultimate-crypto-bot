from abc import ABC, abstractmethod
from collections import deque
from typing import List, Union

from configurations import INDICATOR_WINDOW
from indicators.ema import ExponentialMovingAverage, load_ema


class BaseIndicator(ABC):
    def __init__(self,
                 label: str,
                 window: Union[int, None] = INDICATOR_WINDOW[0],
                 alpha: Union[List[float], float, None] = None):
        """
        Indicator constructor.

        :param window: (int) rolling window used for indicators
        :param alpha: (float) decay rate for EMA; if NONE, raw values returned
        """
        self._label = f"{label}_{window}"
        self.window = window
        if self.window is not None:
            self.all_history_queue = deque(maxlen=self.window + 1)
        else:
            self.all_history_queue = deque(maxlen=2)
        self.ema = load_ema(alpha=alpha)
        self._value = 0.

    def __str__(self):
        return f'Indicator.base() [ window={self.window}, ' \
               f'all_history_queue={self.all_history_queue}, ema={self.ema} ]'

    @abstractmethod
    def reset(self) -> None:
        """
        Clear values in indicator cache.

        :return: (void)
        """
        self._value = 0.
        self.all_history_queue.clear()

    @abstractmethod
    def step(self, **kwargs) -> None:
        """
        Update indicator with steps from the environment.

        :param kwargs: data values passed to indicators
        :return: (void)
        """
        if self.ema is None:
            pass
        elif isinstance(self.ema, ExponentialMovingAverage):
            self.ema.step(**kwargs)
        elif isinstance(self.ema, list):
            for ema in self.ema:
                ema.step(**kwargs)
        else:
            pass

    @abstractmethod
    def calculate(self, *args, **kwargs) -> float:
        """
        Calculate indicator value.

        :return: (float) value of indicator
        """
        pass

    @property
    def value(self) -> Union[List[float], float]:
        """
        Get indicator value for the current time step.

        :return: (scalar float)
        """
        if self.ema is None:
            return self._value
        elif isinstance(self.ema, ExponentialMovingAverage):
            return self.ema.value
        elif isinstance(self.ema, list):
            return [ema.value for ema in self.ema]
        else:
            return 0.

    @property
    def label(self) -> Union[List[str], str]:
        """
        Get indicator value for the current time step.

        :return: (scalar float)
        """
        if self.ema is None:
            return self._label
        elif isinstance(self.ema, ExponentialMovingAverage):
            return f"{self._label}_{self.ema.alpha}"
        elif isinstance(self.ema, list):
            return [f"{self._label}_{ema.alpha}" for ema in self.ema]
        else:
            raise ValueError(f"Error: EMA provided not valid --> {self.ema}")

    @property
    def raw_value(self) -> float:
        """
        Guaranteed raw value, if EMA is enabled.

        :return: (float) raw indicator value
        """
        return self._value

    @staticmethod
    def safe_divide(nom: float, denom: float) -> float:
        """
        Safely perform divisions without throwing an 'divide by zero' exception.

        :param nom: nominator
        :param denom: denominator
        :return: value
        """
        if denom == 0.:
            return 0.
        elif nom == 0.:
            return 0.
        else:
            return nom / denom
