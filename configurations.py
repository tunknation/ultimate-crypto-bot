import logging
import os

import pytz as tz

TIMEZONE = tz.utc

# singleton for logging
logging.basicConfig(level=logging.INFO, format='[%(asctime)s] %(message)s')
LOGGER = logging.getLogger('logs')

# Paths
ROOT_PATH = os.path.dirname(os.path.realpath(__file__))
DATA_PATH = os.path.join(ROOT_PATH, 'data_sets')
TMP_PATH = os.path.join(ROOT_PATH, 'tmp')

# Indicator config
INDICATOR_WINDOW = [60 * i for i in [5, 15]]
INDICATOR_WINDOW_MAX = max(INDICATOR_WINDOW)
INDICATOR_WINDOW_FEATURES = [f'_{i}' for i in [5, 15]]

# Env config
SYMBOL = 'ETH'
FITTING_FILE = os.path.join(DATA_PATH, 'ETH-USD-11-2020.csv.xz')
TESTING_FILE = os.path.join(DATA_PATH, 'ETH-USD-12-2020.csv.xz')
