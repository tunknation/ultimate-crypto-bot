# order.py
#
#   Market order implementations for {broker/position}.py
#
#
from abc import ABC

from configurations import LOGGER


class OrderMetrics(object):
    def __init__(self):
        """
        Class for capturing order / position metrics
        """
        self.drawdown_max = 0.0
        self.upside_max = 0.0
        self.steps_in_position = 0

    def __str__(self):
        return ('OrderMetrics: [ drawdown_max={} | upside_max={} | '
                'steps_in_position={} ]').format(self.drawdown_max, self.upside_max,
                                                 self.steps_in_position)


class Order(ABC):
    _id = 0
    DEFAULT_SIZE = 1000.

    def __init__(self,
                 price: float,
                 step: int,
                 average_execution_price: float,
                 order_type='limit',
                 ccy='ETH-USD',
                 side='long'):
        """

        :param price: (float).
        :param step: (int).
        :param average_execution_price: (float).
        :param order_type: (str).
        :param ccy: (str).
        :param side: (str).
        """
        self.id = Order._id
        self.order_type = order_type
        self.ccy = ccy
        self.side = side
        self.price = price
        self.step = step
        self.average_execution_price = average_execution_price
        self.metrics = OrderMetrics()
        self.executed = 0.
        self.queue_ahead = 0.
        self.executions = dict()
        Order._id += 1

    def __str__(self):
        return ' {} #{} | {} | {:.3f} | {} | {} | {}'.format(
            self.ccy, self.id, self.side, self.price, self.step, self.metrics,
            self.queue_ahead)

    @property
    def is_filled(self) -> bool:
        """
        If TRUE, the entire order has been executed.

        :return: (bool) TRUE if the order is completely filled
        """
        return self.executed >= Order.DEFAULT_SIZE

    def update_metrics(self, price: float, step: int) -> None:
        """
        Update specific position metrics per each order.

        :param price: (float) current midpoint price
        :param step: (int) current time step
        :return: (void)
        """
        self.metrics.steps_in_position = step - self.step
        if self.is_filled:
            if self.side == 'long':
                unrealized_pnl = (price - self.average_execution_price) / \
                                 self.average_execution_price
            elif self.side == 'short':
                unrealized_pnl = (self.average_execution_price - price) / \
                                 self.average_execution_price
            else:
                unrealized_pnl = 0.0
                LOGGER.warning('alert: unknown order.step() side %s' % self.side)

            if unrealized_pnl < self.metrics.drawdown_max:
                self.metrics.drawdown_max = unrealized_pnl

            if unrealized_pnl > self.metrics.upside_max:
                self.metrics.upside_max = unrealized_pnl


class MarketOrder(Order):
    def __init__(self, ccy='ETH-USD', side='long', price=0.0, step=-1):
        super(MarketOrder, self).__init__(price=price,
                                          step=step,
                                          average_execution_price=-1,
                                          order_type='market',
                                          ccy=ccy,
                                          side=side)

    def __str__(self):
        return "[MarketOrder] " + super(MarketOrder, self).__str__()
