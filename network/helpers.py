import torch as T
import torch.nn as nn


class Flatten(nn.Module):
    def forward(self, x):
        return x.view(x.size(0), -1)


def disable_gradients(network):
    # Disable calculations of gradients
    for param in network.parameters():
        param.requires_grad = False


def update_params(optim, network, loss, grad_clip=None, retain_graph=False):
    optim.zero_grad()
    loss.backward(retain_graph=retain_graph)

    if grad_clip is not None:
        for p in network.modules():
            T.nn.utils.clip_grad_norm_(p.parameters(), grad_clip)

    optim.step()


def soft_update(target, source, tau):
    for t, s in zip(target.parameters(), source.parameters()):
        t.data.copy_(t.data * (1.0 - tau) + s.data * tau)


def hard_update(target, source):
    target.load_state_dict(source.state_dict())
