from gym import register
from envs.market_trading_env import MarketTradingEnv, MarketTradingContinuousEnv

register(
    id=MarketTradingEnv.id,
    entry_point='envs:MarketTradingEnv',
    max_episode_steps=1000000,
    nondeterministic=False
)

register(
    id=MarketTradingContinuousEnv.id,
    entry_point='envs:MarketTradingContinuousEnv',
    max_episode_steps=1000000,
    nondeterministic=False
)

