from indicators import ExponentialMovingAverage
from indicators.base_indicator import BaseIndicator
from typing import List, Tuple, Union


class IndicatorManager(object):
    __slots__ = ['indicators']

    def __init__(self):
        """
        Wrapper class to manage multiple indicators at the same time
        (e.g., window size stacking)

        # :param smooth_values: if TRUE, values returned are EMA smoothed, otherwise raw
        #     values indicator values
        """
        self.indicators = list()

    def get_labels(self) -> list:
        """
        Get labels for each indicator being managed.

        :return: List of label names
        """
        # return [label[0] for label in self.indicators]
        labels = []
        for label, indicator in self.indicators:
            indicator_label = indicator.label
            if isinstance(indicator_label, list):
                labels.extend(indicator_label)
            else:
                labels.append(indicator_label)
        return labels

    def add(self, name_and_indicator: Tuple[str, Union[BaseIndicator, ExponentialMovingAverage]]) \
            -> None:
        """
        Add indicator to the list to be managed.

        :param name_and_indicator: tuple(name, indicator)
        :return: (void)
        """
        self.indicators.append(name_and_indicator)

    def delete(self, index: Union[int, None]) -> None:
        """
        Delete an indicator from the manager.

        :param index: index to delete (int or str)
        :return: (void)
        """
        if isinstance(index, int):
            del self.indicators[index]
        else:
            self.indicators.remove(index)

    def pop(self, index: Union[int, None]) -> Union[float, None]:
        """
        Pop indicator from manager.

        :param index: (int) index of indicator to pop
        :return: (name, indicator)
        """
        if index is not None:
            return self.indicators.pop(index)
        else:
            return self.indicators.pop()

    def step(self, **kwargs) -> None:
        """
        Update indicator with new step through environment.

        :param kwargs: Data passed to indicator for the update
        :return:
        """
        for (name, indicator) in self.indicators:
            indicator.step(**kwargs)

    def reset(self) -> None:
        """
        Reset all indicators being managed.

        :return: (void)
        """
        for (name, indicator) in self.indicators:
            indicator.reset()

    def get_value(self) -> List[float]:
        """
        Get all indicator values in the manager's inventory.

        :return: (list of floats) Indicator values for current time step
        """
        values = []
        for name, indicator in self.indicators:
            indicator_value = indicator.value
            if isinstance(indicator_value, list):
                values.extend(indicator_value)
            else:
                values.append(indicator_value)
        return values
