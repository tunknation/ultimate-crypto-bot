import torch as T
from torch.nn import functional as F
from torch.distributions import Categorical

from network import create_linear_network, BaseNetwork, create_dqn_base, calculate_conv_output_dim


class ConvCategoricalPolicy(BaseNetwork):
    def __init__(self, num_channels, num_actions, input_dims, hidden_units=[],
                 initializer='xavier'):
        super().__init__()
        self.base = create_dqn_base(num_channels, initializer)

        self.fc_input_dims = calculate_conv_output_dim(self.base, input_dims)

        self.head = create_linear_network(
            input_dim=self.fc_input_dims,
            output_dim=num_actions,
            hidden_units=hidden_units,
            initializer=initializer)

    def forward(self, states):
        action_probs = self.base(states)
        return action_probs

    def sample(self, states):
        states = self.base(states)

        action_probs = F.softmax(self.head(states), dim=1)
        action_dist = Categorical(action_probs)
        actions = action_dist.sample().view(-1, 1)
        greedy_actions = T.argmax(action_probs, dim=1, keepdim=True)

        # Avoid numerical instability.
        z = (action_probs == 0.0).float() * 1e-8
        log_action_probs = T.log(action_probs + z)

        return actions, action_probs, log_action_probs, greedy_actions
