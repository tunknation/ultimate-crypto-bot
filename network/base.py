import torch as T
import torch.nn as nn

from .helpers import disable_gradients


class BaseNetwork(nn.Module):
    _n = None

    def save(self, path):
        T.save(self.state_dict(), path)

    def load(self, path):
        self.load_state_dict(T.load(path))

    def eval(self):
        return super(BaseNetwork, self).eval().apply(disable_gradients)

    @property
    def num_params(self):
        if self._n is None:
            n = 0
            for p in list(self.parameters()):
                nn = 1
                for s in list(p.size()):
                    nn = nn * s
                n += nn
            self._n = n
        return self._n
