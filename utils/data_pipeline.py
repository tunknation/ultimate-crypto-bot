from datetime import datetime as dt

import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler

from configurations import LOGGER, TIMEZONE
from indicators import apply_ema_all_data, load_ema, reset_ema


class DataPipeline(object):
    def __init__(self, alpha: float or list or None):
        """
        Data Pipeline constructor.
        """
        self.alpha = alpha
        self.ema = load_ema(alpha=alpha)
        self._scaler = StandardScaler()

    def reset(self) -> None:
        """
        Reset data pipeline.
        """
        self._scaler = StandardScaler()
        self.ema = reset_ema(ema=self.ema)

    @staticmethod
    def import_csv(filename: str) -> pd.DataFrame:
        """
        Import an historical tick file.

        :param filename: Full file path including filename
        :return: (panda.DataFrame) historical limit order book data
        """
        start_time = dt.now(tz=TIMEZONE)

        if 'xz' in filename:
            data = pd.read_csv(filepath_or_buffer=filename, index_col=0,
                               compression='xz', engine='c')
        elif 'csv' in filename:
            data = pd.read_csv(filepath_or_buffer=filename, index_col=0, engine='c')
        else:
            LOGGER.warn('Error: file must be a csv or xz')
            data = None

        elapsed = (dt.now(tz=TIMEZONE) - start_time).seconds
        LOGGER.info('Imported %s from a csv in %i seconds' % (filename[-25:], elapsed))
        return data

    def fit_scaler(self, data: pd.DataFrame) -> None:
        """
        Scale data for the neural network.

        :param data: Trading data
        :return: (void)
        """
        self._scaler.fit(data)

    def scale_data(self, data: pd.DataFrame) -> np.ndarray:
        """
        Standardize data.

        :param data: (np.array) all data in environment
        :return: (np.array) normalized observation space
        """
        return self._scaler.transform(data)

    @staticmethod
    def _midpoint_diff(data: pd.DataFrame) -> pd.DataFrame:
        """
        Take log difference of midpoint prices
                log(price t) - log(price t-1)

        :param data: (pd.DataFrame) raw data from LOB snapshots
        :return: (pd.DataFrame) with midpoint prices normalized
        """
        data['midpoint'] = np.log(data['midpoint'].values)
        data['midpoint'] = (data['midpoint'] - data['midpoint'].shift(1)
                            ).fillna(method='bfill')
        return data

    def load_environment_data(self, fitting_file: str, testing_file: str, as_pandas: bool = False) -> (
            pd.DataFrame, pd.DataFrame, pd.DataFrame):
        """
        Import and scale environment data set with prior day's data.

        Midpoint gets log-normalized:
            log(price t) - log(price t-1

        :param fitting_file: trading data t-1
        :param testing_file: trading data t
        :param as_pandas: if TRUE, return data as DataFrame, otherwise np.array
        :return: (pd.DataFrame or np.array) scaled environment data
        """
        # Import data used to fit scaler
        fitting_data = self.import_csv(filename=fitting_file)

        fitting_data = self._midpoint_diff(data=fitting_data)  # normalize
        fitting_data = apply_ema_all_data(ema=self.ema, data=fitting_data)
        self.fit_scaler(fitting_data)

        del fitting_data

        # Import data to normalize and use in environment
        raw_data = self.import_csv(filename=testing_file)

        # Raw midpoint prices for back-testing environment
        midpoint_prices = raw_data['midpoint']

        # Copy of raw LOB snapshots for normalization
        normalized_data = self._midpoint_diff(data=raw_data.copy(deep=True))

        # Derive OFI statistics
        normalized_data = apply_ema_all_data(ema=self.ema, data=normalized_data)

        # Get column names for putting the numpy values into a data frame
        column_names = normalized_data.columns.tolist()
        # Scale data
        normalized_data = self.scale_data(normalized_data)

        # Remove outliers
        normalized_data = np.clip(normalized_data, -10., 10.)

        # Put data in a data frame
        normalized_data = pd.DataFrame(normalized_data,
                                       columns=column_names,
                                       index=midpoint_prices.index)

        if as_pandas is False:
            midpoint_prices = midpoint_prices.to_numpy(dtype=np.float64)
            raw_data = raw_data.to_numpy(dtype=np.float32)
            normalized_data = normalized_data.to_numpy(dtype=np.float32)

        print(normalized_data.tail())

        return midpoint_prices, raw_data, normalized_data
