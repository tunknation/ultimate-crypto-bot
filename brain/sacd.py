import os
import numpy as np
import torch as T
from torch.optim import Adam
from torch.utils.tensorboard import SummaryWriter
import time

from configurations import TMP_PATH
from memory import LazyPrioritizedMultiStepMemory
from network import update_params, disable_gradients
from utils import RunningMeanStatistics, SubprocVecEnv
from q_functions import TwinnedDiscreteConvQNetwork
from policy import ConvCategoricalPolicy


class SACDAgent:
    def __init__(self,
                 env_fn,
                 name=None,
                 epochs=10,
                 seed=0,
                 render=False,
                 workers=1,
                 batch_size=64,
                 lr=0.0003,
                 gamma=0.99,
                 target_entropy_ratio=0.98,
                 multi_step=1,
                 memory_size=300000,
                 log_interval=10,
                 update_interval=20,
                 target_update_interval=8000,
                 max_ep_len=10000,
                 num_steps_per_epoch=100000,
                 num_start_steps=20000,
                 num_eval_eps=10):
        self.name = name if name else f'sacd-{seed}-{int(time.time())}'
        self.model_dir = os.path.join(TMP_PATH, self.name, 'model')
        self.summary_dir = os.path.join(TMP_PATH, self.name, 'summary')

        if not os.path.exists(self.model_dir):
            os.makedirs(self.model_dir)
        if not os.path.exists(self.summary_dir):
            os.makedirs(self.summary_dir)

        self.epochs = epochs
        self.seed = seed
        self.render = render
        self.workers = workers
        self.batch_size = batch_size
        self.gamma_n = gamma ** multi_step
        self.num_start_steps = num_start_steps
        self.num_steps_per_epoch = num_steps_per_epoch // workers
        self.total_num_steps = (num_steps_per_epoch * epochs) // workers
        self.update_interval = update_interval // workers
        self.target_update_interval = target_update_interval // workers
        self.num_eval_eps = num_eval_eps
        self.max_ep_len = max_ep_len
        self.log_interval = log_interval

        self.global_steps = 0
        self.learning_steps = 0
        self.local_steps = 0
        self.episodes = 0
        self.best_eval_score = -np.inf

        # Set environments
        self.env = SubprocVecEnv([env_fn for _ in range(workers)])
        self.eval_env = env_fn()

        self.n_actions = self.eval_env.action_space.n
        self.obs_shape = self.eval_env.observation_space.shape

        # Set seed
        T.manual_seed(seed)
        np.random.seed(seed)
        self.env.seed(seed)
        self.eval_env.seed(2 ** 31 - 1 - seed)

        self.device = T.device('cuda:0' if T.cuda.is_available() else 'cpu')

        # LazyMemory efficiently stores FrameStacked states
        beta_steps = self.total_num_steps / self.update_interval
        self.memory = LazyPrioritizedMultiStepMemory(
            capacity=memory_size,
            state_shape=self.obs_shape,
            device=self.device,
            gamma=gamma,
            multi_step=multi_step,
            parallel=workers,
            beta_steps=beta_steps)

        # Loggers and statistics
        self.writer = SummaryWriter(log_dir=self.summary_dir)
        self.train_ret = RunningMeanStatistics(log_interval)
        self.train_len = RunningMeanStatistics(log_interval)
        self.train_time = RunningMeanStatistics(log_interval)

        # Define networks
        self.policy = ConvCategoricalPolicy(
            num_channels=self.obs_shape[0],
            num_actions=self.n_actions,
            input_dims=(self.obs_shape),
            hidden_units=[512]).to(self.device)
        self.online_critic = TwinnedDiscreteConvQNetwork(
            num_channels=self.obs_shape[0],
            output_dim=self.n_actions,
            input_dims=(self.obs_shape),
            hidden_units=[512]).to(device=self.device)
        self.target_critic = TwinnedDiscreteConvQNetwork(
            num_channels=self.obs_shape[0],
            output_dim=self.n_actions,
            input_dims=(self.obs_shape),
            hidden_units=[512]).to(device=self.device).eval()

        # Copy parameters of the learning network to the target network
        self.target_critic.load_state_dict(self.online_critic.state_dict())

        # Disable gradient calculations of the target network
        disable_gradients(self.target_critic)

        # Setup optimizers
        self.policy_optim = Adam(self.policy.parameters(), lr=lr)
        self.q1_optim = Adam(self.online_critic.Q1.parameters(), lr=lr)
        self.q2_optim = Adam(self.online_critic.Q2.parameters(), lr=lr)

        # Target entropy is -log(1/|A|) * ratio (= maximum entropy * ratio)
        self.target_entropy = -np.log(1.0 / self.n_actions) * target_entropy_ratio

        # We optimize log(alpha), instead of alpha
        self.log_alpha = T.zeros(1, requires_grad=True, device=self.device)
        self.alpha = self.log_alpha.exp()
        self.alpha_optim = Adam([self.log_alpha], lr=lr)

        # Load checkpoint
        if os.path.exists(os.path.join(self.model_dir, 'final')):
            self._load_checkpoint(os.path.join(self.model_dir, 'final'))

    def explore(self, obs):
        # Act with randomness
        obs = T.ByteTensor(obs[None, ...]).to(self.device).float()
        with T.no_grad():
            action, _, _ = self.policy.sample(obs)
        return action.item()

    def exploit(self, obs):
        # Act without randomness
        obs = T.ByteTensor(obs[None, ...]).to(self.device).float()
        with T.no_grad():
            _, _, _, action = self.policy.act(obs)
        return action.item()

    def run(self):
        obs, ep_ret, ep_len, start_time = self.env.reset(), 0, 0, time.time()

        for step in range(self.total_num_steps):
            # Until start steps have elapsed, randomly sample actions
            # from a uniform distribution for better exploration. Afterwards,
            # use the learned policy.
            if self.num_start_steps > self.global_steps:
                action = [self.env.action_space.sample() for _ in range(self.workers)]
            else:
                action = [self.explore(o) for o in obs]

            # Step the env
            obs_, reward, done, _ = self.env.step(action)
            self.global_steps += 1 * self.workers
            ep_len += 1
            ep_ret += np.mean(reward)

            for o, a, r, o_, d in zip(obs, action, reward, obs_, done):
                # Ignore the "done" signal if it comes from hitting the time
                # horizon (that is, when it's an artificial terminal signal
                # that isn't based on the agent's state)
                d = False if ep_len == self.max_ep_len else d

                # Remember experience
                self.memory.append(o, a, r, o_, d)

            # most recent obs!
            obs = obs_

            # End of trajectory handling
            if done.any() or (ep_len == self.max_ep_len):
                # We log running mean of training rewards
                self.train_ret.append(ep_ret)
                self.train_len.append(ep_len)
                self.train_time.append(time.time() - start_time)

                # Reset episode variables
                obs, ep_ret, ep_len, start_time = self.env.reset(), 0, 0, time.time()

            # Update handling
            if step % self.update_interval == 0 and self.global_steps >= self.num_start_steps:
                self._learn()

            # Update target handling
            if step % self.target_update_interval == 0:
                self._update_target()

            # End of epoch handling
            if (step + 1) % self.num_steps_per_epoch == 0:
                epoch = (step + 1) // self.num_steps_per_epoch

                # Save model
                self._save_checkpoint(os.path.join(self.model_dir, 'final'))

                # Test the performance of the deterministic version of the agent
                self._evaluate_agent()

                self.writer.add_scalar(
                    'reward/train', self.train_ret.get(), self.global_steps)

                print(('-' * 25), f'EPOCH-{epoch}', ('-' * 25))
                print(f'EpRet: {self.train_ret.get()} ')
                print(f'EpLen: {self.train_len.get()} ')
                print(f'EpTime: {self.train_time.get()}')
                print('=' * 75)

    def _evaluate_agent(self):
        start_time = time.time()
        ep, steps, total_ret = 0, 0, 0

        for i in range(self.num_eval_eps):
            obs, done, ep_ret, ep_len = self.eval_env.reset(), False, 0, 0

            while (not done) and ep_len <= self.max_ep_len:
                action = self.exploit(obs)
                obs_, reward, done, _ = self.eval_env.step(action)

                if self.render:
                    self.eval_env.render()

                steps += 1
                ep_len += 1
                ep_ret += reward
                obs = obs_

            ep += 1
            total_ret += ep_ret

        mean_ret = total_ret / ep
        mean_time = (time.time() - start_time) / ep

        if mean_ret > self.best_eval_score:
            self.best_eval_score = mean_ret
            self._save_checkpoint(os.path.join(self.model_dir, 'best'))

        self.writer.add_scalar(
            'reward/test', mean_ret, self.global_steps)

        print(('-' * 25), 'EVAL', ('-' * 25))
        print(f'EpRet: {mean_ret} ')
        print(f'EpLen: {steps} ')
        print(f'EpTime: {mean_time}')
        print('=' * 75)

    def _learn(self):
        assert hasattr(self, 'q1_optim') and hasattr(self, 'q2_optim') and \
               hasattr(self, 'policy_optim') and hasattr(self, 'alpha_optim')

        if self.memory.pointer < self.batch_size:
            return

        self.learning_steps += 1

        batch, weights = self.memory.sample(self.batch_size)

        q1_loss, q2_loss, errors, mean_q1, mean_q2 = self._calc_critic_loss(batch, weights)
        policy_loss, entropies = self._calc_policy_loss(batch, weights)
        entropy_loss = self._calc_entropy_loss(entropies, weights)

        update_params(self.q1_optim, None, q1_loss)
        update_params(self.q2_optim, None, q2_loss)
        update_params(self.policy_optim, None, policy_loss)
        update_params(self.alpha_optim, None, entropy_loss)

        self.alpha = self.log_alpha.exp()

        self.memory.update_priority(errors)

        if self.learning_steps % self.log_interval == 0:
            self.writer.add_scalar(
                'loss/Q1', q1_loss.detach().item(),
                self.learning_steps)
            self.writer.add_scalar(
                'loss/Q2', q2_loss.detach().item(),
                self.learning_steps)
            self.writer.add_scalar(
                'loss/policy', policy_loss.detach().item(),
                self.learning_steps)
            self.writer.add_scalar(
                'loss/alpha', entropy_loss.detach().item(),
                self.learning_steps)
            self.writer.add_scalar(
                'stats/alpha', self.alpha.detach().item(),
                self.learning_steps)
            self.writer.add_scalar(
                'stats/mean_Q1', mean_q1, self.learning_steps)
            self.writer.add_scalar(
                'stats/mean_Q2', mean_q2, self.learning_steps)
            self.writer.add_scalar(
                'stats/entropy', entropies.detach().mean().item(),
                self.learning_steps)

    def _update_target(self):
        self.target_critic.load_state_dict(self.online_critic.state_dict())

    def _calc_current_q(self, states, actions, _rewards, _next_states, _dones):
        curr_q1, curr_q2 = self.online_critic(states)
        curr_q1 = curr_q1.gather(1, actions.long())
        curr_q2 = curr_q2.gather(1, actions.long())
        return curr_q1, curr_q2

    def _calc_target_q(self, _states, _actions, rewards, next_states, dones):
        with T.no_grad():
            _, action_probs, log_action_probs = self.policy.sample(next_states)
            next_q1, next_q2 = self.target_critic(next_states)
            next_q = (action_probs * (
                    T.min(next_q1, next_q2) - self.alpha * log_action_probs
            )).sum(dim=1, keepdim=True)

        assert rewards.shape == next_q.shape
        return rewards + (1.0 - dones) * self.gamma_n * next_q

    def _calc_critic_loss(self, batch, weights):
        curr_q1, curr_q2 = self._calc_current_q(*batch)
        target_q = self._calc_target_q(*batch)

        # TD errors for updating priority weights
        errors = T.abs(curr_q1.detach() - target_q)

        # We log means of Q to monitor training
        mean_q1 = curr_q1.detach().mean().item()
        mean_q2 = curr_q2.detach().mean().item()

        # Critic loss is mean squared TD errors with priority weights
        q1_loss = T.mean((curr_q1 - target_q).pow(2) * weights)
        q2_loss = T.mean((curr_q2 - target_q).pow(2) * weights)

        return q1_loss, q2_loss, errors, mean_q1, mean_q2

    def _calc_policy_loss(self, batch, weights):
        states, actions, rewards, next_states, dones = batch

        # (Log of) probabilities to calculate expectations of Q and entropies
        _, action_probs, log_action_probs = self.policy.sample(states)

        with T.no_grad():
            # Q for every actions to calculate expectations of Q
            q1, q2 = self.online_critic(states)

        # Expectations of entropies
        entropies = -T.sum(
            action_probs * log_action_probs, dim=1, keepdim=True)

        # Expectations of Q.
        q = T.sum(T.min(q1, q2) * action_probs, dim=1, keepdim=True)

        # Policy objective is maximization of (Q + alpha * entropy) with
        # priority weights
        policy_loss = (weights * (- q - self.alpha * entropies)).mean()

        return policy_loss, entropies.detach()

    def _calc_entropy_loss(self, entropies, weights):
        assert not entropies.requires_grad

        # Intuitively, we increse alpha when entropy is less than target
        # entropy, vice versa.
        entropy_loss = -T.mean(
            self.log_alpha * (self.target_entropy - entropies)
            * weights)
        return entropy_loss

    def _save_checkpoint(self, save_dir):
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

        print('-' * 25, 'SAVING CHECKPOINT', '-' * 25)

        T.save({
            'episodes': self.episodes,
            'global_steps': self.global_steps,
            'learning_steps': self.learning_steps,
            'best_eval_score': self.best_eval_score
        }, os.path.join(save_dir, 'state.pkl'))

        self.policy.save(os.path.join(save_dir, 'policy.pth'))
        self.online_critic.save(os.path.join(save_dir, 'online_critic.pth'))
        self.target_critic.save(os.path.join(save_dir, 'target_critic.pth'))

        T.save(self.policy_optim.state_dict(), os.path.join(save_dir, 'policy_optim.pth'))
        T.save(self.q1_optim.state_dict(), os.path.join(save_dir, 'q1_optim.pth'))
        T.save(self.q2_optim.state_dict(), os.path.join(save_dir, 'q2_optim.pth'))

        T.save(self.log_alpha, os.path.join(save_dir, 'log_alpha.pth'))
        T.save(self.alpha, os.path.join(save_dir, 'alpha.pth'))
        T.save(self.alpha_optim.state_dict(), os.path.join(save_dir, 'alpha_optim.pth'))

    def _load_checkpoint(self, load_dir):
        if not os.path.exists(load_dir):
            return

        print('-' * 25, 'LOADING CHECKPOINT', '-' * 25)

        state = T.load(os.path.join(load_dir, 'state.pkl'))
        self.episodes = state['episodes']
        self.global_steps = state['global_steps']
        self.learning_steps = state['learning_steps']
        self.best_eval_score = state['best_eval_score']

        self.policy.load(os.path.join(load_dir, 'policy.pth'))
        self.online_critic.load(os.path.join(load_dir, 'online_critic.pth'))
        self.target_critic.load(os.path.join(load_dir, 'target_critic.pth'))

        self.policy_optim.load_state_dict(T.load(os.path.join(load_dir, 'policy_optim.pth')))
        self.q1_optim.load_state_dict(T.load(os.path.join(load_dir, 'q1_optim.pth')))
        self.q2_optim.load_state_dict(T.load(os.path.join(load_dir, 'q2_optim.pth')))

        self.log_alpha.data.copy_(T.load(os.path.join(load_dir, 'log_alpha.pth')).data)
        self.alpha.data.copy_(T.load(os.path.join(load_dir, 'alpha.pth')).data)
        self.alpha_optim.load_state_dict(T.load(os.path.join(load_dir, 'alpha_optim.pth')))

    def __del__(self):
        self.env.close()
        self.eval_env.close()
        self.writer.close()
