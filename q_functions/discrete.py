from network import BaseNetwork, create_dqn_base, create_linear_network, calculate_conv_output_dim


class DiscreteConvQNetwork(BaseNetwork):
    def __init__(self, num_channels, output_dim, input_dims, hidden_units=[], initializer='xavier'):
        super(DiscreteConvQNetwork, self).__init__()

        self.base = create_dqn_base(num_channels, initializer=initializer)

        self.fc_input_dim = calculate_conv_output_dim(self.base, input_dims)

        self.a_head = create_linear_network(
            input_dim=self.fc_input_dim,
            output_dim=output_dim,
            hidden_units=hidden_units,
            initializer=initializer)
        self.v_head = create_linear_network(
            input_dim=self.fc_input_dim,
            output_dim=1,
            hidden_units=hidden_units,
            initializer=initializer)

    def forward(self, states):
        h = self.base(states)
        a = self.a_head(h)
        v = self.v_head(h)
        return v + a - a.mean(1, keepdim=True)


class TwinnedDiscreteConvQNetwork(BaseNetwork):
    def __init__(self, num_channels, output_dim, input_dims, hidden_units=[], initializer='xavier'):
        super(TwinnedDiscreteConvQNetwork, self).__init__()

        self.Q1 = DiscreteConvQNetwork(
            num_channels, output_dim, input_dims, hidden_units, initializer)
        self.Q2 = DiscreteConvQNetwork(
            num_channels, output_dim, input_dims, hidden_units, initializer)

    def forward(self, states):
        Q1 = self.Q1(states)
        Q2 = self.Q2(states)
        return Q1, Q2
